var scopes = require('unity-js-scopes')
var http = require('http');
var crypto = require('crypto');

var query_host = "m.bulbapedia.bulbagarden.net"
var pokemon_list = "/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number?action=raw"
var pokemon_page_end = "_(Pok%C3%A9mon)"
var pokemon_page_start = "/wiki/"
var pokemon_icons = "http://cdn.bulbagarden.net/upload/";
var cry_host = "http://www.legendarypokemon.net/dp_pokecry/";

var pokemon_list_generation_group_regex = /\[\[(Generation [IVX]+)\]\]=+((.|[\n\r])+?)==/gm
var pokemon_list_regex = /^{{rdex\|[\dCMeo]*\|(\d+)\|([a-zA-Zé\s\.\'\-\d♀♂]+)\|\d\|([a-zA-Z\|]+)}}$/gm

var clean_wiki_markup_regex = /[\[{]{2}(?:[^\|}\]]*\|)*([^}\]]+)[}\]]{2}/gm

var details_block_regex = /{{Pok(?:é|Ã©)mon Infobox.*[\n\r]*((.+=.+[\n\r])+)/g
var details_regex = /(.*)=(.*)\s?\|/g

var biology_block_regex = /==Biology==[\n\r]*((.|[\n\r])+?)==/g

var pokedex_entries_regex = /{{Dex\/Entry(\d)\|((?:v\d?=([^\|]+)\|)*).*entry=(.+\.)}}$/gm

var location_entries_regex = /\{{2}Availability\/Entry(\d)\|(?:\d\|)?((?:v\d?=([^\|]+)\|)*).*area=(.+)\}{2}/g
var location_routes_regex = /\{{2}rtn?\|(\d+)\|([^}]+)\}{2}/g

var stats_block_regex = /{{.*Stats(([^}]*|[\n\r])+)/g
var base_stats_regex = /([A-Z].+)=\s*(\d+)/g

var moves_regex = /learnlist\/[^|]+\|(?:((?:TM|HM)?\d+)\|)+([^\|]+)\|([^\|]+)/g

var evolutions_block_regex = /{{(?:E|e)vobox(?:-\d)?[\n\r]((.*=.*[\n\r])+)/g
var evolutions_regex = /name\d[a-z]?=(.*)/g

var effectiveness_block_regex = /\{{2}TypeEffectiveness((?:[^}]|[\n\r])*)/g

var details_filter = {
    "type" : "\tType :\t\t",
    "category" : "\tCategory :\t",
    "mega" : "\tMega :\t\t",
    "height-m" : "\tHeight (m) :\t",
    "weight-kg" : "\tWeight (kg) :\t",
    "ability" : "\tAbility :\t\t",
    "egggroup" : "\tEgg Group :\t",

    "EXCLUSIONS" : [
        "typebox",
        "abilityn",
        "egggroupn"
    ]
}

var get_image_url = function(imageName)
{
    var md5sum = crypto.createHash("md5");
    md5sum.write (imageName);
    var hash = md5sum.digest('hex').substring(0,2);

    return pokemon_icons + hash[0] + "/" + hash + "/" + imageName;
}

var get_image_url_scaled = function(imageName, size)
{
    var md5sum = crypto.createHash("md5");
    md5sum.write (imageName);
    var hash = md5sum.digest('hex').substring(0,2);

    return pokemon_icons + "thumb/" + hash[0] + "/" + hash + "/" + imageName + "/" + size.toString() + "px-" + imageName;
}

var cleanWikiMarkup = function (str, passes)
{
    for (var i = 0; i < passes; i++)
    {
        str = str.replace (clean_wiki_markup_regex, "$1");
    }
    return str;
}

var POKEMON_TEMPLATE =
        {
    "schema-version": 1,
    "template": {
        "category-layout": "grid",
        "card-layout": "horizontal",
        "card-size": "small"
    },
    "components": {
        "title": "title",
        "art": {
            "field": "art",
        },
        "subtitle": "subtitle"
    }
}

var scopeID = -1;

scopes.self.initialize(
            {}
            ,
            {
                run: function() {
                    console.log('Running...')
                },
                start: function(scope_id) {
                    console.log('Starting scope id: '
                                + scope_id
                                + ', '
                                + scopes.self.scope_directory)
                    scopeID = scope_id;
                },
                search: function(canned_query, metadata) {
                    return new scopes.lib.SearchQuery(
                                canned_query,
                                metadata,
                                // run
                                function(search_reply) {
                                    var qs = canned_query.query_string();

                                    var get_pokemon_list = function(response) {
                                        var res = '';

                                        // Another chunk of data has been recieved, so append it to res
                                        response.on('data', function(chunk) {
                                            res += chunk;
                                        });

                                        // The whole response has been recieved
                                        response.on('end', function() {
                                            try {
                                                var category_renderer = new scopes.lib.CategoryRenderer(JSON.stringify(POKEMON_TEMPLATE));

                                                var match = pokemon_list_generation_group_regex.exec (res);

                                                qs = qs.toUpperCase();

                                                while (match !== null) {

                                                    var category = search_reply.register_category(match[1], match[1], "", category_renderer);

                                                    var pokemon_entry = pokemon_list_regex.exec(match[2])

                                                    while (pokemon_entry)
                                                    {
                                                        if (!(qs === "" || qs === "TYPE")){
                                                            if (pokemon_entry[1].toUpperCase().indexOf(qs) === -1 &&
                                                                pokemon_entry[2].toUpperCase().indexOf(qs) === -1 &&
                                                                pokemon_entry[3].toUpperCase().indexOf(qs) === -1) {

                                                                pokemon_entry = pokemon_list_regex.exec(match[2])
                                                                continue;
                                                            }
                                                        }

                                                        var categorised_result = new scopes.lib.CategorisedResult(category);

                                                        categorised_result.set_uri("/wiki/"+encodeURIComponent(pokemon_entry[2])+pokemon_page_end);
                                                        categorised_result.set_title(pokemon_entry[2]);
                                                        categorised_result.set("subtitle", pokemon_entry[1]);

                                                        var icon_name = pokemon_entry[1] + "MS.png";

                                                        categorised_result.set_art(get_image_url(icon_name));

                                                        search_reply.push(categorised_result);
                                                        pokemon_entry = pokemon_list_regex.exec(match[2]);
                                                    }

                                                    match = pokemon_list_generation_group_regex.exec (res);
                                                }
                                                search_reply.finished();
                                            }
                                            catch(e) {
                                                // Forecast not available
                                                console.log("Forecast for '" + qs + "' is unavailable: " + e)
                                            }
                                        });
                                    }

                                    http.request({host: query_host, path: pokemon_list}, get_pokemon_list).end();
                                },
                                // cancelled
                                function() {
                                });
                },
                preview: function(result, action_metadata) {
                    return new scopes.lib.PreviewQuery(
                                result,
                                action_metadata,
                                // run
                                function(preview_reply) {
                                    var layout1col = new scopes.lib.ColumnLayout(1);
                                    var layout2col = new scopes.lib.ColumnLayout(2);
                                    var layout3col = new scopes.lib.ColumnLayout(3);
                                    layout1col.add_column(["header", "image", "summary", "details", "pokedex", "locations", "base_stats", "effectiveness", "attacks", "evolutions", "action"]);

                                    layout2col.add_column(["header","image"]);
                                    layout2col.add_column(["summary", "details", "pokedex", "locations", "base_stats", "effectiveness", "attacks", "evolutions", "action"]);

                                    layout3col.add_column(["header","image"]);
                                    layout3col.add_column(["summary", "details", "pokedex", "locations", "base_stats", "effectiveness", "attacks", "evolutions", "action"]);
                                    layout3col.add_column([]);

                                    preview_reply.register_layout([layout1col, layout2col, layout3col]);

                                    var header = new scopes.lib.PreviewWidget("header", "header");
                                    header.add_attribute_mapping("title", "title");
                                    header.add_attribute_mapping("subtitle", "subtitle");

                                    var description = new scopes.lib.PreviewWidget("summary", "expandable");
                                    var details = new scopes.lib.PreviewWidget("details", "expandable");
                                    var pokedex = new scopes.lib.PreviewWidget("pokedex", "expandable");
                                    var locations = new scopes.lib.PreviewWidget("locations", "expandable");
                                    var base_stats = new scopes.lib.PreviewWidget("base_stats", "expandable");
                                    var attacks = new scopes.lib.PreviewWidget("attacks", "expandable");
                                    var evolutions = new scopes.lib.PreviewWidget("evolutions", "expandable");
                                    var effectiveness = new scopes.lib.PreviewWidget("effectiveness", "expandable");


                                    var image = new scopes.lib.PreviewWidget("image", "image");
                                    var image_url = result.get("subtitle") + result.get("title") + ".png"
                                    image_url = image_url.replace(/[\u2642\u2640]/g, "");
                                    image.add_attribute_value("source", get_image_url_scaled(image_url, 256));
                                    console.log(get_image_url_scaled(image_url, 256));


                                    //var cry = new scopes.lib.PreviewWidget("cry", "audio");
                                    //cry.add_attribute_value("tracks",
                                    //                        {
                                    //                            "title":"Cry",
                                    //                            "source":cry_host + result.get("subtitle").replace(/^0*/, "") + ".mp3"
                                    //                        });
                                    //

                                    var action = new scopes.lib.PreviewWidget("action", "actions");
                                    action.add_attribute_value(
                                                    "actions",
                                                    {
                                                        "id": "wiki",
                                                        "label": "Bulbapedia",
                                                        "uri": "http://" + query_host + result.get("uri")
                                                    }
                                                );

                                    var get_pokemon_preview = function(response) {
                                        var res = '';

                                        response.on('data', function(chunk) {
                                            res += chunk;
                                        });

                                        response.on('end', function() {
                                            try {

                                                // Details Start
                                                try
                                                {
                                                    details.add_attribute_value("title", "Details");

                                                    details_block_regex.lastIndex = 0;
                                                    match = details_block_regex.exec (res);

                                                    if (match) {

                                                        var detail = details_regex.exec(match[1]);

                                                        while (detail)
                                                        {
                                                            var validDetail = false;
                                                            var key_to_use = ""

                                                            for (var key in details_filter)
                                                            {
                                                                validDetail |= (detail[1].indexOf(key) > -1);
                                                                if (validDetail) {
                                                                    key_to_use = key;
                                                                    break;
                                                                }
                                                            }

                                                            validDetail &= details_filter["EXCLUSIONS"].indexOf(detail[1]) === -1;

                                                            if (validDetail)
                                                            {
                                                                var ing_action = new scopes.lib.PreviewWidget(detail[1], "text");

                                                                ing_action.add_attribute_value("text", details_filter[key_to_use] + cleanWikiMarkup(detail[2], 1));

                                                                details.add_widget (ing_action);
                                                            }
                                                            detail = details_regex.exec (match[1]);
                                                        }
                                                    }
                                                }
                                                catch (e) {}
                                                // Details End

                                                // Biology Start
                                                description.add_attribute_value("title", "Description");

                                                try
                                                {
                                                    biology_block_regex.lastIndex = details_block_regex.lastIndex;
                                                    var match = biology_block_regex.exec(res);

                                                    if (match) {
                                                        var des = cleanWikiMarkup(match[1], 1);

                                                        var dectription_text = new scopes.lib.PreviewWidget("Biology", "text");
                                                        dectription_text.add_attribute_value("text", des);
                                                        description.add_widget (dectription_text);
                                                    } else {
                                                        description.add_attribute_value("text", "");
                                                    }
                                                }
                                                catch (e){}
                                                // Biology End

                                                // Pokedex Start
                                                try
                                                {
                                                    pokedex.add_attribute_value("title", "Pokedex Entries");

                                                    pokedex_entries_regex.lastIndex = biology_block_regex.lastIndex;
                                                    match = pokedex_entries_regex.exec (res);

                                                    while (match) {

                                                        if (!match[2])
                                                        {
                                                            match = pokedex_entries_regex.exec (res);
                                                            continue;
                                                        }

                                                        var entry_action = new scopes.lib.PreviewWidget(match[2], "text");

                                                        var games = match[3];
                                                        var numberOfGames = parseInt(match[1]);
                                                        if (numberOfGames > 1)
                                                        {
                                                            games = "";
                                                            var parts = match[2].split("|");
                                                            for (var part in parts)
                                                            {
                                                                if (part < numberOfGames)
                                                                {
                                                                    var index = parts[part].indexOf("=") + 1;
                                                                    games += parts[part].substring(index);
                                                                    games += ", ";
                                                                }
                                                            }
                                                            games = games.substring (0, games.length-2);
                                                        }

                                                        var entry = cleanWikiMarkup(match[4], 1);
                                                        entry_action.add_attribute_value("title", games)
                                                        entry_action.add_attribute_value("text", entry);

                                                        pokedex.add_widget (entry_action);
                                                        match = pokedex_entries_regex.exec (res);
                                                    }
                                                }
                                                catch (e) {}
                                                // Pokedex End

                                                // Locations Start
                                                try
                                                {
                                                    locations.add_attribute_value("title", "Locations");

                                                    location_entries_regex.lastIndex = pokedex_entries_regex.lastIndex;
                                                    match = location_entries_regex.exec (res);

                                                    while (match) {

                                                        if (!match[2])
                                                        {
                                                            match = location_entries_regex.exec (res);
                                                            continue;
                                                        }

                                                        try
                                                        {
                                                            var location_action = new scopes.lib.PreviewWidget(match[2], "text");

                                                            // Games
                                                            var games = match[3];
                                                            var numberOfGames = parseInt(match[1]);
                                                            if (numberOfGames > 1)
                                                            {
                                                                games = "";
                                                                var parts = match[2].split("|");
                                                                for (var part in parts)
                                                                {
                                                                    if (part < numberOfGames)
                                                                    {
                                                                        var index = parts[part].indexOf("=") + 1;
                                                                        games += parts[part].substring(index);
                                                                        games += ", ";
                                                                    }
                                                                }
                                                                games = games.substring (0, games.length-2);
                                                            }

															
                                                            var locations_string = match[4];
                                                            locations_string = locations_string.replace (location_routes_regex, "Route $1 ($2)");
                                                            locations_string = cleanWikiMarkup(locations_string, 2);

                                                            // Widget
                                                            location_action.add_attribute_value("text", locations_string);
                                                            location_action.add_attribute_value("title", games)
                                                            locations.add_widget (location_action);
                                                        }
                                                        catch(e) {}
                                                        finally
                                                        {
                                                            match = location_entries_regex.exec (res);
                                                        }
                                                    }
                                                }
                                                catch (e) {}
                                                // Locations End

                                                // Stats Start
                                                {
                                                    base_stats.add_attribute_value("title", "Base Stats");

                                                    stats_block_regex.lastIndex = location_entries_regex.lastIndex;
                                                    var stats_block = stats_block_regex.exec(res);

                                                    if (stats_block)
                                                    {
                                                        var stat = base_stats_regex.exec(stats_block[1]);

                                                        while (stat)
                                                        {
                                                            var stat_action = new scopes.lib.PreviewWidget(stat[1], "text");
                                                            stat_action.add_attribute_value("text", "\t" + stat[1] + " :\t" + stat[2]);
                                                            base_stats.add_widget (stat_action);

                                                            stat = base_stats_regex.exec(stats_block[1]);
                                                        }
                                                    }
                                                }
                                                // Stats End

                                                // Type Effectiveness Start
                                                {

                                                    effectiveness.add_attribute_value("title", "Type Effectiveness");

                                                    effectiveness_block_regex.lastIndex = stats_block_regex.lastIndex;
                                                    var effectiveness_block = effectiveness_block_regex.exec(res);

                                                    var neutral_group = "";
                                                    var strong_group = "";
                                                    var weak_group = "";
                                                    var immune_group = "";

                                                    if (effectiveness_block)
                                                    {
                                                        var te = base_stats_regex.exec(effectiveness_block[1]);
                                                        var tabbing_width = 16;

                                                        while (te)
                                                        {
                                                            var te_value = parseInt (te[2]) * 0.01;
                                                            var tabbs = tabbing_width-te[1].length;
                                                            var te_string = "\t" + te[1] + " :\tx" + te_value.toString() + "\n";

                                                            if (te_value === 1)
                                                                neutral_group += te_string;
                                                            else if (te_value > 1)
                                                                weak_group += te_string;
                                                            else if (te_value === 0)
                                                                immune_group += te_string;
                                                            else if (te_value < 1)
                                                                strong_group += te_string;

                                                            te = base_stats_regex.exec(effectiveness_block[1]);
                                                        }
                                                    }

                                                    if (strong_group)
                                                    {
                                                        var strong_table = new scopes.lib.PreviewWidget("Strong Against", "text");
                                                        strong_table.add_attribute_value ("title", "Resistant To");
                                                        strong_table.add_attribute_value ("text", strong_group.substring(0, strong_group.length-1));
                                                        effectiveness.add_widget (strong_table);
                                                    }

                                                    if (weak_group)
                                                    {
                                                        var weak_table = new scopes.lib.PreviewWidget("Weak Against", "text");
                                                        weak_table.add_attribute_value ("title", "Weak Against");
                                                        weak_table.add_attribute_value ("text", weak_group.substring(0, weak_group.length-1));
                                                        effectiveness.add_widget (weak_table);
                                                    }

                                                    if (immune_group)
                                                    {
                                                        var immune_table = new scopes.lib.PreviewWidget("Immune To", "text");
                                                        immune_table.add_attribute_value ("title", "Immune To");
                                                        immune_table.add_attribute_value ("text", immune_group.substring(0, immune_group.length-1));
                                                        effectiveness.add_widget (immune_table);
                                                    }

                                                    if (neutral_group)
                                                    {
                                                        var neutral_table = new scopes.lib.PreviewWidget("Neutral", "text");
                                                        neutral_table.add_attribute_value ("title", "Neutral");
                                                        neutral_table.add_attribute_value ("text", neutral_group.substring(0, neutral_group.length-1));
                                                        effectiveness.add_widget (neutral_table);
                                                    }
                                                }
                                                // Type Effectiveness End

                                                // Attacks Start
                                                try
                                                {
                                                    attacks.add_attribute_value("title", "Attacks");

                                                    moves_regex.lastIndex = effectiveness_block_regex.lastIndex;
                                                    var atk = moves_regex.exec(res);

                                                    while (atk)
                                                    {
                                                        var attack_action = new scopes.lib.PreviewWidget(atk[0], "text");
                                                        attack_action.add_attribute_value("text", "\t" + atk[1] + " :\t" + atk[2] + " (" + atk[3] + ")");
                                                        attacks.add_widget (attack_action);

                                                        atk = moves_regex.exec(res);
                                                    }
                                                }
                                                catch (e) {}
                                                // Attacks End

                                                // Evolutions Start
                                                {
                                                    var evo_actions = new Array();
                                                    evolutions.add_attribute_value("title", "Evolution Family");

                                                    evolutions_block_regex.lastIndex = moves_regex.lastIndex;
                                                    var evo_block = evolutions_block_regex.exec(res);

                                                    if (evo_block)
                                                    {
                                                        var evo = evolutions_regex.exec(evo_block[1]);
                                                        var i = 0;

                                                        while (evo)
                                                        {
                                                            evo_actions[i] = new scopes.lib.PreviewWidget(evo[0], "icon-actions");

                                                            var q = new scopes.lib.CannedQuery (scopeID, evo[1], "");

                                                            evo_actions[i].add_attribute_value("actions",
                                                                                               {
                                                                                                   "id": evo[1] + "_action",
                                                                                                   "label": evo[1],
                                                                                                   "uri": q.to_uri()
                                                                                               });
                                                            evolutions.add_widget (evo_actions[i++]);

                                                            evo = evolutions_regex.exec(evo_block[1]);
                                                        }
                                                    }
                                                    preview_reply.push(evo_actions);
                                                }
                                                // Evolutions End
                                            }
                                            catch(e) {
                                                console.log("Preview for '" + result.get("header") + "' is unavailable: " + e)
                                            }
                                            finally
                                            {
                                                preview_reply.push([description, details, pokedex, locations, base_stats, effectiveness, attacks, evolutions]);
                                                preview_reply.finished();
                                            }
                                        });
                                    }

                                    preview_reply.push([header, image, action]);
                                    http.request({host: query_host, path: result.get("uri") + "?action=raw"}, get_pokemon_preview).end();
                                },
                                // cancelled
                                function() {
                                });
                }
            }
            );

